package com.ssm.controller;

import com.ssm.pojo.Book;
import com.ssm.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    @Qualifier("bookServiceImp")
    private BookService bookService;

    @RequestMapping("/allBook")
    public String listAllBook(Model model) {
        List<Book> book = bookService.findBook();
        model.addAttribute("list", book);
        return "allBook";
    }

    @RequestMapping("/toAddBook")
    public String toAddBook() {
        return "/addBook";
    }

    @RequestMapping("/addBook")
    public String addBook(Book book) {
        bookService.addBook(book);
        return "redirect:/book/allBook";
    }

    @RequestMapping("/toUpdate/{bookID}")
    public String toUpdate(@PathVariable("bookID") int id, Model model) {
        System.out.println("toUpdate=>" + id);
        Book book = bookService.findBookByID(id);
        System.out.println("toUpdate=>" + book);
        model.addAttribute("updatedBook", book);
        return "bookUpdate";
    }

    @RequestMapping("/update")
    public String update(Book book) {
        bookService.updateBook(book);
        return "redirect:/book/allBook";
    }

    @RequestMapping("/delete/{bookID}")
    public String deleteBook(@PathVariable("bookID") int id) {
        bookService.removeBookByID(id);
        return "redirect:/book/allBook";
    }

    @RequestMapping("/queryBookByName")
    public String queryBookByName(String queryBookName, Model model) {

        Book book = bookService.findBookByName(queryBookName);
        System.out.println("==============" + queryBookName + "===========");
        model.addAttribute("book", book);
        return "queryBook";
    }

}
