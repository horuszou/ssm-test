package com.ssm.dao;

import com.ssm.pojo.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    //add
    int addBook(Book book);

    //delete
    int removeBookByID(@Param("bookID") int id);

    //update
    int updateBook(Book book);

    //query
    Book findBookByID(@Param("bookID") int id);

    List<Book> findBook();

    Book findBookByName(@Param("bookName") String bookName);
}
