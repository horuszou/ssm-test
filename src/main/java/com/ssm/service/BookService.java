package com.ssm.service;

import com.ssm.pojo.Book;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookService {
    //add
    int addBook(Book book);

    //delete
    int removeBookByID(int id);

    //update
    int updateBook(Book book);

    //query
    Book findBookByID(int id);

    List<Book> findBook();

    Book findBookByName(String name);
}
