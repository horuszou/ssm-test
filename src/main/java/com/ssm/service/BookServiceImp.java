package com.ssm.service;

import com.ssm.dao.BookMapper;
import com.ssm.pojo.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


public class BookServiceImp implements BookService {

    private BookMapper bookMapper;

    public BookMapper getBookMapper() {
        return bookMapper;
    }

    public void setBookMapper(BookMapper bookMapper) {
        this.bookMapper = bookMapper;
    }

    public int addBook(Book book) {
        return bookMapper.addBook(book);
    }

    public int removeBookByID(int id) {
        return bookMapper.removeBookByID(id);
    }

    public int updateBook(Book book) {
        return bookMapper.updateBook(book);
    }

    public Book findBookByID(int id) {
        return bookMapper.findBookByID(id);
    }

    public List<Book> findBook() {
        return bookMapper.findBook();
    }

    @Override
    public Book findBookByName(String name) {
        return bookMapper.findBookByName(name);
    }


}
