import com.ssm.controller.BookController;
import com.ssm.pojo.Book;
import com.ssm.service.BookService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class MyTest {
    @Test
    public void test() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        BookService bookServiceImp = applicationContext.getBean("bookServiceImp", BookService.class);
        List<Book> book = bookServiceImp.findBook();
        for (Book book1 : book) {
            System.out.println(book1);
        }
//        bookServiceImp.addBook(new Book(4,"fadf",3,"faga"));
    }
}
