<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<form action="${pageContext.request.contextPath}/book/addBook" method="get">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="书籍名称" aria-describedby="basic-addon1" name="bookName">
    </div>

    <div class="input-group">
        <input type="text" class="form-control" placeholder="书籍数量" aria-describedby="basic-addon2" name="bookCounts">
    </div>

    <div class="input-group">
        <input type="text" class="form-control" name="details">
    </div>

    <button type="submit">提交</button>

</form>

</body>
</html>
