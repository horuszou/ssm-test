<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <title>Title</title>
    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<h3>书籍展示</h3>
<br/>
<form action="${pageContext.request.contextPath}/book/queryBookByName" method="get">
    <input type="text" name="queryBookName" placeholder="请输入要查询的书籍名称" class="activeTableTab">
    <input type="submit" value="查询" class="ui-button">
</form>
<table>
    <thead>
    <tr>
        <th>书籍编号</th>
        <th>书籍名称</th>
        <th>书籍数量</th>
        <th>书籍详情</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="book" items="${list}">
        <tr>
            <td>${book.bookID}</td>
            <td>${book.bookName}</td>
            <td>${book.bookCounts}</td>
            <td>${book.details}</td>
            <td>
                <a href="${pageContext.request.contextPath}/book/toUpdate/${book.bookID}">修改</a>
                &nbsp; | &nbsp;
                <a href="${pageContext.request.contextPath}/book/delete/${book.bookID}">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<br/>
<a href="${pageContext.request.contextPath}/book/toAddBook">添加书籍</a>
</body>
</html>
