<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<h3>书籍修改</h3>
<form action="${pageContext.request.contextPath}/book/update" method="get">

    <input type="hidden" name="bookID" value="${updatedBook.bookID}">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="书籍名称" value="${updatedBook.bookName}" name="bookName">
    </div>

    <div class="input-group">
        <input type="text" class="form-control" placeholder="书籍数量" value="${updatedBook.bookCounts}" name="bookCounts">
    </div>

    <div class="input-group">
        <input type="text" class="form-control" name="details" value="${updatedBook.details}">
    </div>

    <button type="submit">提交</button>

</form>

</body>
</html>
