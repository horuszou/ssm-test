<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2020/9/23
  Time: 15:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<h3>书籍查询</h3>
<table>
    <thead>
    <tr>
        <th>书籍编号</th>
        <th>书籍名称</th>
        <th>书籍数量</th>
        <th>书籍详情</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>${book.bookID}</td>
        <td>${book.bookName}</td>
        <td>${book.bookCounts}</td>
        <td>${book.details}</td>
        <td>
            <a href="${pageContext.request.contextPath}/book/toUpdate/${book.bookID}">修改</a>
            &nbsp; | &nbsp;
            <a href="${pageContext.request.contextPath}/book/delete/${book.bookID}">删除</a>
        </td>
    </tr>
    </tbody>
</table>

</body>
</html>
